#!/usr/bin/env python
# -*- coding: utf-8 -*-

# https://www.w3.org/Protocols/rfc2616/rfc2616-sec5.html
# https://www.w3.org/Protocols/rfc2616/rfc2616-sec6.html

import sys
from socket import (socket, AF_INET, SOCK_STREAM)
from threading import Thread
import os 
MAX_RECV_BYTES = 1024

def parseReq(reqBytes):
    request = {}
    reqStr = reqBytes.decode('UTF-8')
    headerBody = reqStr.split('\r\n\r\n')
    lines = headerBody[0].split('\r\n')
    requestLine = lines[0]
    request['method'] = requestLine.split(' ')[0]
    request['URI'] = requestLine.split(' ')[1]
    request['httpVersion'] = requestLine.split(' ')[2]
    request['path'] = (request['URI']).split('?')[0]
    if request['path'] == '/':
        request['path'] = '/index.html'
    listOfHeaders = lines[1:]
    request['headers'] = {}
    for h in listOfHeaders:
        request['headers'][('').join(h.split(':')[:1])] = ((':').join((h.split(':')[1:]))).strip()
    if request['method'] == 'POST':
        request['body'] = headerBody[1]
    return request

def dumpRes(resDict):
    out = ""
    statusLine = resDict['httpVersion'] + " " + str(resDict['statusCode']) + " " + resDict['reasonPhrase'] + "\r\n"
    headers = ""
    for key, value in (resDict['headers']).items():
        headers += str(key) + ": " + str(value) + "\r\n"
    if (resDict['statusCode'] < 300) and (resDict['body'] != None):
        out += statusLine + headers + "\r\n" + resDict['body']
    else:
        out += statusLine + headers + "\r\n"
    outBytes = out.encode('ascii')
    return outBytes

class Client(Thread):

    __ip = None
    __port = None
    __socket = None
    __close = False

    def __init__(self, socketAdressTuple):
        Thread.__init__(self)
        (self.__socket, (self.__ip, self.__port)) = socketAdressTuple
        print("New client connected from ip {} port {}".format(self.__ip, self.__port))

    def run(self):
        data = self.__socket.recv(MAX_RECV_BYTES)
        request = parseReq(data)
        responseBytes = None
        responseData = None
        response = {}
        response['headers'] = {}
        response['body'] = None
        if request['method'] == 'GET':
            try:
                with open((request['path']).strip('/')) as f:
                    responseData = f.read()
                response['body'] = responseData
                response['httpVersion'] = 'HTTP/1.0'
                response['statusCode'] = 200
                response['reasonPhrase'] = 'ok'
                response['headers']['Content-Length'] = len(responseData)
            except Exception as a:
                response['httpVersion'] = 'HTTP/1.0'
                response['statusCode'] = 500
                response['reasonPhrase'] = 'Internal Server Error'
        elif request['method'] == 'POST':
            path = (request['path']).strip('/')
            if len(path.split('/')) > 1:
                dirsPath = ('/').join(path.split('/')[:-1])
                filePath = path.split('/')[-1]
                completePath = ('/').join([dirsPath, filePath])
                try:
                    if not os.path.exists(dirsPath):
                        os.makedirs(dirsPath)
                    with open(completePath, 'w') as f:
                        f.write(request['body'])
                    response['httpVersion'] = 'HTTP/1.0'
                    response['statusCode'] = 200
                    response['reasonPhrase'] = 'ok'
                except Exception as a:
                    response['httpVersion'] = 'HTTP/1.0'
                    response['statusCode'] = 500
                    response['reasonPhrase'] = 'Internal Server Error'
            else:
                completePath = filePath = path
                try:
                    with open(completePath, 'w') as f:
                        f.write(request['body'])
                    response['httpVersion'] = 'HTTP/1.0'
                    response['statusCode'] = 200
                    response['reasonPhrase'] = 'ok'
                except Exception as a:
                    response['httpVersion'] = 'HTTP/1.0'
                    response['statusCode'] = 500
                    response['reasonPhrase'] = 'Internal Server Error'
        else:
            print("This server only implements GET and POST methods")
            response['httpVersion'] = 'HTTP/1.0'
            response['statusCode'] = 501
            response['reasonPhrase'] = 'Not Implemented'
        responseBytes = dumpRes(response)
        self.__socket.send(responseBytes)
        self.__socket.close()

def main(args):
    sock = None
    try:
        PORT = 80 # HTTP default port
        IP = '0.0.0.0' # Serve on undefined interface
        for i in args:
            arg = i.split('=')
            if arg[0] == '--port':
                PORT = int(arg[1])
            if arg[0] == '--ip':
                IP = arg[1]
        print("Using ip {} and port {}. For diferent values use --port=<port> and --ip=<ip>".format(IP, PORT))

        sockAddress = (IP, PORT)
        sock = socket(AF_INET, SOCK_STREAM)
        sock.bind(sockAddress)
        sock.listen(1)
        print("Server listening on {}, port {}".format(IP, PORT))
        while True:
            clientSocketAddressTuple = sock.accept() # (socket, (ip, port))
            client = Client(clientSocketAddressTuple)
            client.start()
    except KeyboardInterrupt as e:
        sock.close()
        print("Exiting...")

if __name__ == '__main__':
    sys.exit(main(sys.argv))