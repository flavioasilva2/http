#!/usr/bin/env python
# -*- coding: utf-8 -*-
# https://docs.python.org/3/library/urllib.parse.html
# https://www.w3.org/Protocols/rfc2616/rfc2616-sec5.html
# https://www.w3.org/Protocols/rfc2616/rfc2616-sec6.html#sec6

import sys
from socket import (socket, AF_INET, SOCK_STREAM, gethostbyname)
from urllib.parse import urlparse

MAX_RECV_BYTES = 1024

def dumpReq(request):
    out = {}
    if request['path'] == '':
        request['path'] = '/'
    requestLine = ""
    requestLine += request['method'] + " " + request['path'] + " " + request['httpVersion'] + "\r\n"
    rawHeaders = ""
    for key, value in (request['headers']).items():
        rawHeaders += str(key) + ": " + str(value) + "\r\n"
    out['msg'] = requestLine + rawHeaders + "\r\n" 
    if request['method'] == 'POST':
        out['msg'] += request['body']
    out['msg'] = (out['msg']).encode('ascii')
    host = request['headers']['host']
    srv = host.split(':')[0]
    port = 80
    if host.split(':')[0] != host.split(':')[-1]:
        port = int(host.split(':')[-1])
    ip = gethostbyname(srv)
    out['addr'] = (ip, port)
    return out

def parseRes(resBytes):
    response = resBytes.decode('UTF-8')
    out = {}
    out['headers'] = {}
    responseHeaders = response.split('\r\n\r\n')[0]
    if response.split('\r\n\r\n')[-1] != responseHeaders:
        out['body'] = response.split('\r\n\r\n')[-1]
    statusLine = responseHeaders.split('\r\n')[0]
    out['httpVersion'] = statusLine.split(' ')[0]
    out['statusCode'] = int(statusLine.split(' ')[1])
    if statusLine.split(' ')[-1] != statusLine.split(' ')[0]:
        out['reasonPhrase'] = (' ').join(statusLine.split(' ')[2:])
    rawHeaders = responseHeaders.split('\r\n')[1:]
    for h in rawHeaders:
        out['headers'][h.split(':')[0]] = (':').join(h.split(":")[1:]).strip()
    return out

def main(args):
    if len(args) < 2:
        print("\nMissing url, use\n\n{} <url>\n\nwhere <url> is of the form\n\nhttp:[//host[:port]][/path][?query][#fragment]\n\n--post=<data> or --post=@/path/to/file to use POST method\n".format(args[0]))
    request = {}
    request['headers'] = {}
    parseResult = urlparse(args[1])
    if parseResult.scheme != 'http':
        print("Malformed URL please use the following scheme:\n\nhttp:[//host[:port]][/path][?query][#fragment]\n\n")
    postData = None
    printRaw = False
    for arg in args:
        a = arg.split("=")
        if a[0] == '--post':
            postData = a[1]
        if a[0] == '--raw':
            printRaw = True    
    if postData != None:
        request['method'] = 'POST'
        if postData[0] == '@':
            with open(postData[1:]) as f:
                request['body'] = f.read()
        else:
            request['body'] = postData
        request['headers']['content-length'] = len(request['body'])
    else:
        request['method'] = 'GET'
    request['httpVersion'] = 'HTTP/1.0'
    request['path'] = parseResult.path
    request['headers']['host'] = parseResult.netloc
    req = dumpReq(request)
    sock = socket(AF_INET, SOCK_STREAM)
    try:
        sock.connect(req['addr'])
    except ConnectionRefusedError as e:
        print("Connection refused\n")
        sock.close()
        return 1
    sock.send(req['msg'])
    responseData = sock.recv(MAX_RECV_BYTES)
    response = parseRes(responseData)
    sock.close()

    if printRaw:
        print('\nrequest:\n')
        print(req)
        print('\nresponse:\n')
        print(responseData)
        print('\n\n')

    print(response['body'])

if __name__ == '__main__':
    sys.exit(main(sys.argv))